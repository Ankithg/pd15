#include<stdio.h>
int sum_digits (int n);
int main()
{
 int n, result;
 printf("Enter the value of n\n");
 scanf("%d",&n);
 result=sum_digits(n);
 printf("%d",result);
}
int sum_digits(int num)
{ 
 int a,sum=0;
 while(num>0)
 {
  a=num%10;
  sum=sum+a;
  num=num/10;
 }
 return sum;
} 