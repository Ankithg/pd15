#include<stdio.h>
void swap(int *a,int *b)
{
 int temp;
 temp=*a;
 *a=*b;
 *b=temp;
 printf("In function(call_by_ref) a=%d,b=%d \n",*a,*b);
}
int main()
{
 int a=3,b=4;
 printf("In main a=%d and b=%d \n",a,b);
 swap(&a,&b);
 printf("In main,a=%d,b=%d \n",a,b);
 return 0;
}
 